#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os
import re

def check_guest(user, period):
    contribution_events = user.events.list(as_list=False)
    print("%s has been active in the last %s days, checking events." % (user.username, str(period)))
    for event in contribution_events:
        event_dt = datetime.strptime(event.created_at, '%Y-%m-%dT%H:%M:%S.%f%z')
        delta_event = datetime.now(tz) - event_dt
        if delta_event.days > period:
            return True
        for action in ["pushed", "accepted", "closed", "approved", "reopened", "destroyed", "merged", "imported"]:
            if action in event.action_name:
                return False
        if event.action_name == "opened" and event.target_type == "MergeRequest":
                return False
    # no events found in period
    #print("Parsed all events for %s and did not find non-guest actions." % user.username)
    return True

def has_memberships(user):
    memberships = user.memberships.list(as_list=False)
    if memberships:
        return True
    return False

parser = argparse.ArgumentParser(description='Identify potential Guest users on GitLab self-managed')
parser.add_argument('gitlaburl', help='URL of the GitLab instance')
parser.add_argument('token', help='Admin API token to read the requested users')
parser.add_argument('--check_period', help='Period in days for which to check activities for.', type=int, default=180)
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token.strip())

try:
    users = gl.users.list(as_list=False)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

tz = datetime.now().astimezone().tzinfo

is_bot = lambda account: re.match('(project|group)_.*_bot', account.username)
nonbot_accounts = list(filter(lambda account: not is_bot(account), users))

period = args.check_period

inactive_users = []
no_memberships = []
potential_guest = []
total_users = 0

for user in nonbot_accounts:
    if user.attributes["username"] in ["support-bot","alert-bot","ghost"]:
        continue
    if user.attributes["state"] != "active":
        continue
    if "current_sign_in_at" not in user.attributes:
        print("Cannot access user field 'current_sign_in_at', use an admin token.")
        exit(1)
    else:
        total_users += 1
        current_sign_in = user.attributes["current_sign_in_at"]
        last_activity = user.attributes["last_activity_on"]
        inactive = True
        if current_sign_in is not None:
            current_sign_in_dt = datetime.strptime(current_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
            delta_sign_in = datetime.now(tz) - current_sign_in_dt
            if delta_sign_in.days < period:
                inactive = False
        if last_activity is not None:
            last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
            delta_activity = datetime.now() - last_activity_dt
            if delta_activity.days < period:
                inactive = False
        if inactive:
            inactive_users.append(user.attributes)
        else:
            if not has_memberships(user):
                no_memberships.append(user.attributes)
            else:
                if check_guest(user, period):
                    potential_guest.append(user.attributes)

print("%s total active users on the instance" % str(total_users))
print("%s users have NO activity in the last %s days" % (str(len(inactive_users)), str(period)))
print("%s users are not members of any groups or projects" % str(len(no_memberships)))
print("%s users have NO user events above Guest permissions in the last %s days" % (str(len(potential_guest)), str(period)))
print("NOTE: Not all events are tracked, so the list of potential guest users may contain users executing actions that require higher permissions.")
print("For example, environment actions, viewing dashboards, cancelling jobs, managing variables, viewing merge requests etc are all not tracked as events and thus can not be checked by this report.")
print("Please view the potential guest user list as rough estimate, based on those users not pushing, creating or approving merge requests and not destroying environments.")

reportfilepath = "report/inactive_users_%s.csv" % str(datetime.now().date())

os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","current_sign_in_at","last_activity_on"]
    reportwriter.writerow(fields)
    for user in inactive_users:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

reportfilepath = "report/no_memberships_users_%s.csv" % str(datetime.now().date())

os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","current_sign_in_at","last_activity_on"]
    reportwriter.writerow(fields)
    for user in no_memberships:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

reportfilepath = "report/potential_guest_users_%s.csv" % str(datetime.now().date())

os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","current_sign_in_at","last_activity_on"]
    reportwriter.writerow(fields)
    for user in potential_guest:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)
